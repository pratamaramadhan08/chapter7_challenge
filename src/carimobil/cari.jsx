import {Navbar, NavbarBrand,NavbarToggler, Collapse, Nav, Button, Card, Col, Container, Form, Row } from 'reactstrap'; 
import axios from 'axios';
import React, { useEffect, useRef, useState } from 'react';

import { Navigate } from 'react-router-dom';
import { addUser } from '../slices/userSlice';
import { useDispatch } from "react-redux";

const FilterCar = () => { 
    const dispatch = useDispatch();
    const [isLoggedIn, setIsLoggedIn] = useState(true);
    const [user, setUser] = useState({});
    const [cars, setCars] = useState([]);
    const capacityField = useRef();
    const isWithDriverField = useRef();
    const availableAtDateField = useRef();
    const availableAtTimeField = useRef();


    useEffect(() => {

        const fetchData = async () => {
            try {
                // Check status user login
                // 1. Get token from localStorage
                const token = localStorage.getItem("token");

                // 2. Check token validity from API
                const currentUserRequest = await axios.get(
                    "http://localhost:3500/auth/me",
                    {
                        headers: {
                            Authorization: `Bearer ${token}`,
                        },
                    }
                );

                const currentUserResponse = currentUserRequest.data;

                if (currentUserResponse.status) {
                    dispatch(
                        addUser({
                            user: currentUserResponse.data.user,
                            token: token,
                        })
                    )

                        setUser(currentUserResponse.data.user);
                }
            } catch (err) {
                setIsLoggedIn(false);
            }
        };

        fetchData();

    }, []);

    const logout = () => {
        localStorage.removeItem("token");

        setIsLoggedIn(false);
        setUser({});
    };

    const filtered = async (e) => {
        e.preventDefault();
        try {

            const dateTime = new Date(`${availableAtDateField.current.value} ${availableAtTimeField.current.value}`)

            const dataCars = await axios.get(`http://localhost:3500/cars/filtered?isWithDriver=${isWithDriverField.current.value}&capacity=${capacityField.current.value}&availableAt=${dateTime.toISOString()}`)

            const payloadData = await dataCars.data.data.filteredCars;
            console.log(dataCars);
            setCars(payloadData);
        } catch (err) {
            console.log(err);
        }
    }
    return isLoggedIn ? (
<div>
    <div>
        <div>
        <div> 
            <Navbar 
            color="light" 
            expand="md" 
            light 
            container 
            header 
            > 
                <NavbarBrand href="/"> 
                    <img alt='' src='img/logo.png'/> 
                </NavbarBrand> 
                <NavbarToggler onClick={function noRefCheck(){}} /> 
                <Collapse navbar> 
                    <Nav 
                    className="ms-auto" 
                    navbar-nav 
                    > 
                    <ul className="navbar-nav ms-auto"> 
                        <li className="nav-item"> 
                        <a className="nav-link text-black mrml-10" href="/cariMobil">Our Services</a> 
                        </li> 
                        <li className="nav-item"> 
                        <a className="nav-link text-black mrml-10" href="#WhyUs">Why Us</a> 
                        </li> 
                        <li className="nav-item"> 
                        <a className="nav-link text-black mrml-10" href="#Testi">Testimonial</a> 
                        </li> 
                        <li className="nav-item"> 
                        <a className="nav-link text-black mrml-10" href="#FAQ">FAQ</a> 
                        </li>
                        <li className="nav-item"> 
                        <a className="nav-link text-black mrml-10" href="#faq">hello kak! {user.username}</a> 
                        </li> 
                        <button onClick={(e) => logout(e)} style={{color:"aliceblue", textDecoration: "none"}} >Log Out</button>
                    </ul> 
                    </Nav> 
                </Collapse> 
                </Navbar> 
        </div> 

        <div className="container">
            <div className="row navbar navbar-expand-lg navbar-light bg-light">
                <div className="col-md-6">
                <br />
                <br />
                <h1 className="tulisan1">
                    Sewa & Rental Mobil Terbaik di kawasan Karanganyar
                </h1>
                <p className="tulisan2">
                    Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas
                    terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu
                    untuk sewa mobil selama 24 jam.
                </p>
                </div>
                <div className="col-md-6 pe-auto">
                <img
                    src="img/img_car.png"
                    width="700px"
                    alt="loading"
                    className="img-fluid"
                />
                </div>
            </div>
            </div>
            </div>
            <div className="container mt-30k ">
                    <div className="sewa2">
                        <div className="card garis1 br-15 cardcari ml-0k mr-0k">
                            <Form onSubmit={(e) => filtered(e)}>
                                <div className="row">
                                    <div className="col-md-11 tengah">

                                        <div className="row">

                                            <div className="col-md-3 tengah mt-15">
                                                <div className=" form-group">
                                                    <label className="form-label">Tipe Driver</label>
                                                    <select ref={isWithDriverField} className="form-select">
                                                        <option value="true">Dengan Sopir</option>
                                                        <option value="false">Tanpa Sopir</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div className="col-md-3 tengah mt-15">
                                                <div className=" form-group">
                                                    <label className="form-label">Tanggal</label>
                                                    <input ref={availableAtDateField} type="date" className="form-control" />
                                                </div>
                                            </div>

                                            <div className="col-md-3 tengah mt-15">
                                                <div className="form-group ">
                                                    <label className="form-label">Waktu Jemput/Ambil</label>
                                                    <select id="inputTime" className="form-select" ref={availableAtTimeField}>
                                                        <option selected hidden>Waktu Jemput</option>
                                                        <option value="08:00">08:00 WIB</option>
                                                        <option value="09:00">09:00 WIB</option>
                                                        <option value="10:00">10:00 WIB</option>
                                                        <option value="11:00">11:00 WIB</option>
                                                        <option value="12:00">12:00 WIB</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div className="col-md-3 tengah mt-15">
                                                <div className=" form-group">
                                                    <label className="form-label">Jumlah Penumpang</label>
                                                    <select ref={capacityField} className="form-select">
                                                        <option hidden>Jumlah Penumpang</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div className="col-md-1 tengah ">
                                        <button type="submit" id="load-btn" className=" btn-success2 bwhite1  bold1 ">Cari Mobil</button>
                                    </div>
                                </div>
                            </Form>
                        </div>
                    </div>
                </div>

        
        </div>
        <Container>
                    <Row>
                        {cars.map((car) => (
                            <Col md={4}>
                                <Card style={{ marginTop: "2rem" }} key={car.id}>
                                    <img src={car.image} alt="" style={{ height: "250px" }} />
                                    <div className="card-body">
                                        <p>
                                            {car.model} / {car.manufacture}
                                        </p>
                                        <h5 className="card-title bold">
                                            Rp {car.rentPerDay} / hari
                                        </h5>
                                        <p className="card-text">{car.description}</p>
                                        <div className="">
        
                                            {car.capacity} Orang
                                        </div>
                                        <div className="pt-2">
    
                                            {car.transmission}
                                        </div>
                                        <div className="pt-2">
                                
                                            Tahun {car.year}
                                        </div>
                                        <Button variant="success" className=" w-100 mt-3">
                                            Pilih Mobil
                                        </Button>
                                    </div>
                                </Card>
                            </Col>
                        ))}
                    </Row>
                </Container>
        <div className="container mb-5" style={{marginTop: "12%"}}>
                <div className="row">
                <div className="col-sm">
                    <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                    <p>binarcarrental@gmail.com</p>
                    <p>081-233-334-808</p>
                </div>
                <div className="col-sm-2 pl-5">
                    <h6>Our services</h6>
                    <h6 className="pt-2">Why Us</h6>
                    <h6 className="pt-2">Testimonial</h6>
                    <h6 className="pt-2">FAQ</h6>
                </div>
                <div className="col-sm pl-5">
                    <p>Connect with us</p>
                    <div style={{wordSpacing: "1em"}} className="simbol">
                    <img alt="" src="img/icon_facebook.png" width="40px"/>
                    <img alt="" src="img/icon_instagram.png" width="40px"/>
                    <img alt="" src="img/icon_twitter.png" width="40px"/>
                    <img alt="" src="img/icon_mail.png" width="40px"/>
                    <img alt="" src="img/icon_twitch.png" width="40px"/>
                    </div>
                </div>
                <div className="col-sm pl-5 bwh">
                    <p>Copyright Binar 2022</p>
                    <img alt="" style={{marginRight: "20px"}} src="img/logo.png" width="120px"/>
                </div>
                </div>
            </div>
</div>

      ) : (
        <Navigate to="/login" replace />
    );
}
export default FilterCar