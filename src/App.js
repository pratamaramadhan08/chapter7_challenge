import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { Route, Routes } from 'react-router-dom';
import Landing from './landing';
import Halamanfilter from './carimobil/cari';
import Login from './login/login';
import Register from './register/register';
import { Provider } from 'react-redux';
import { store } from '../src/app/store';

function App() {
  return (
    <div className="App">
    <Provider store={store}>
      <Routes>
        <Route path='/' element={<Landing/>} />
        <Route path='/Filtercar' element={<Halamanfilter/>} />
        <Route path='/login' element={<Login/>} />
        <Route path='/register' element={<Register/>} />
      </Routes>
      </Provider>
    </div>
  );
}

export default App;
