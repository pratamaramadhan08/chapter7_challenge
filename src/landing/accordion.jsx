import React from "react"
 
const Accordion = () => (
    <div className="frequenly mlmr-70">
      <div className="container">
        <div className="row">

          <div className="col-md-4 hihi">
            <h4 className="bold">Frequently Asked Question</h4>
            <p>Lorem ipsum sit amet, consectetur elit</p>
          </div>

          <div className="col-md-8">

            <div className="accordion" id="accordionExample">

              <div className="accordion-item mb-20">
                <h4 className="accordion-header" id="headingOne">
                  <button className="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne"
                    aria-expanded="true" aria-controls="collapseOne">
                    Apa saja syarat yang dibutuhkan?
                  </button>
                </h4>
                <div id="collapseOne" className="accordion-collapse collapse show" aria-labelledby="headingOne"
                  data-bs-parent="#accordionExample">
                  <div className="accordion-body">
                    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eligendi voluptate quia nisi fugiat
                      saepe, unde dolores omnis inventore quaerat.</p>
                  </div>
                </div>
              </div>

              <div className="accordion-item mb-20">
                <h4 className="accordion-header" id="heading2">
                  <button className="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse2"
                    aria-expanded="true" aria-controls="collapse2">
                    Berapa hari minimal sewa mobil lepas kunci?
                  </button>
                </h4>
                <div id="collapse2" className="accordion-collapse collapse show" aria-labelledby="heading2"
                  data-bs-parent="#accordionExample">
                  <div className="accordion-body">
                    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eligendi voluptate quia nisi fugiat
                      saepe, unde dolores omnis inventore quaerat.</p>
                  </div>
                </div>
              </div>

              <div className="accordion-item mb-20">
                <h4 className="accordion-header" id="heading3">
                  <button className="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse3"
                    aria-expanded="true" aria-controls="collapse3">
                    Berapa hari sebelumnya sabaiknya booking sewa mobil?
                  </button>
                </h4>
                <div id="collapse3" className="accordion-collapse collapse show" aria-labelledby="heading3"
                  data-bs-parent="#accordionExample">
                  <div className="accordion-body">
                    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eligendi voluptate quia nisi fugiat
                      saepe, unde dolores omnis inventore quaerat.</p>
                  </div>
                </div>
              </div>

              <div className="accordion-item mb-20">
                <h4 className="accordion-header" id="heading4">
                  <button className="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse4"
                    aria-expanded="true" aria-controls="collapse4">
                    Apakah Ada biaya antar-jemput?
                  </button>
                </h4>
                <div id="collapse4" className="accordion-collapse collapse show" aria-labelledby="heading4"
                  data-bs-parent="#accordionExample">
                  <div className="accordion-body">
                    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eligendi voluptate quia nisi fugiat
                      saepe, unde dolores omnis inventore quaerat.</p>
                  </div>
                </div>
              </div>

              <div className="accordion-item mb-20">
                <h4 className="accordion-header" id="heading5">
                  <button className="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse5"
                    aria-expanded="true" aria-controls="collapse5">
                    Bagaimana jika terjadi kecelakaan
                  </button>
                </h4>
                <div id="collapse5" className="accordion-collapse collapse show" aria-labelledby="heading5"
                  data-bs-parent="#accordionExample">
                  <div className="accordion-body">
                    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eligendi voluptate quia nisi fugiat
                      saepe, unde dolores omnis inventore quaerat.</p>
                  </div>
                </div>
              </div>

            </div>

          </div>
     </div>
     </div> 
     </div>
    


)
export default Accordion