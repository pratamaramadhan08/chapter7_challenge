import React from "react";

const Cewek = () => (
  
    <div className="container">
<br /><br />
    <div className="row justify-content-between">
      <div className="col-lg-6 d-flex align-items-center justify-content-center about-img">
        <img src="img/cewe.png" width="450px" className="img-fluid" alt=""/>
      </div>
      <div className="col-lg-6 pt-4 pt-lg-0 order-2 order-lg-1 content cewek">
        <h3>Best Car Rental for any kind of trip in Karanganyar!</h3>
        <p>
          Sewa mobil di Karanganyar bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain,
          kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.
        </p>
        <div className="baris2">
          <img style={{width: "20px"}} src="img/ctg.png" className="me-2" alt=""/>Sewa Mobil Dengan Supir di Bali 12 Jam
        </div>
        <div className="baris2 pt-3">
          <img style={{width: "20px"}} src="img/ctg.png" className="me-2" alt=""/>Sewa Mobil Lepas Kunci di Bali 24 Jam
        </div>
        <div className="baris2 pt-3">
          <img style={{width: "20px"}} src="img/ctg.png" className="me-2" alt=""/>Sewa Mobil Jangka Panjang Bulanan
        </div>
        <div className="baris2 pt-3">
          <img style={{width: "20px"}} src="img/ctg.png" className="me-2" alt=""/>Gratis Antar - Jemput Mobil di Bandara
        </div>
        <div className="baris2 pt-3">
          <img style={{width: "20px"}} src="img/ctg.png" className="me-2" alt=""/>Layanan Airport Transfer / Drop In Out
        </div>
      </div>
    </div>

  </div>
)
export default Cewek