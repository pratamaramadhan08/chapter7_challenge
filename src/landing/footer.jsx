import React from "react"

const Footer = () => (
<div class="container mb-5" style={{marginTop: "12%"}}>
      <div class="row">
        <div class="col-sm">
          <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
          <p>binarcarrental@gmail.com</p>
          <p>081-233-334-808</p>
        </div>
        <div class="col-sm-2 pl-5">
          <h6>Our services</h6>
          <h6 class="pt-2">Why Us</h6>
          <h6 class="pt-2">Testimonial</h6>
          <h6 class="pt-2">FAQ</h6>
        </div>
        <div class="col-sm pl-5">
          <p>Connect with us</p>
          <div style={{wordSpacing: "1em"}} class="simbol">
            <img alt="" src="img/icon_facebook.png" width="40px"/>
            <img alt="" src="img/icon_instagram.png" width="40px"/>
            <img alt="" src="img/icon_twitter.png" width="40px"/>
            <img alt="" src="img/icon_mail.png" width="40px"/>
            <img alt="" src="img/icon_twitch.png" width="40px"/>
          </div>
        </div>
        <div class="col-sm pl-5 bwh">
          <p>Copyright Binar 2022</p>
          <img alt="" style={{marginRight: "20px"}} src="img/logo.png" width="120px"/>
        </div>
      </div>
    </div>


)
export default Footer