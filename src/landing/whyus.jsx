import React from "react";

const Whyus = () => (
    <div className="whyus  mt-50">
    <div className="container">
      <div className="mb-30 hihi">
        <h4 className="bold">Why Us ?</h4>
        <span className="">Mengapa harus pilih Binar Car Rental?</span>
      </div>


      <div className="row">
        <div className="col-md-3">
          <div className="card mt-2 br  ">
            <div className="card-body">
              <img alt="" src="img/1.png" width="30px"/>
              <h5 className="bold mt-20">Mobil Lengkap</h5>
              <p className="mt-20">Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
            </div>
          </div>
        </div>

        <div className="col-md-3">
          <div className="card mt-2 br ">
            <div className="card-body">
              <img alt="" src="img/2.png" width="30px"/>
              <h5 className="bold mt-20">Mobil Lengkap</h5>
              <p className="mt-20">Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
            </div>
          </div>
        </div>


        <div className="col-md-3">
          <div className="card mt-2 br ">
            <div className="card-body">
              <img alt="" src="img/3.png" width="30px"/>
              <h5 className="bold mt-20">Mobil Lengkap</h5>
              <p className="mt-20">Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
            </div>
          </div>
        </div>

        <div className="col-md-3">
          <div className="card mt-2 br ">
            <div className="card-body">
              <img alt="" src="img/4.png" width="30px"/>
              <h5 className="bold mt-20">Mobil Lengkap</h5>
              <p className="mt-20">Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
)

export default Whyus
