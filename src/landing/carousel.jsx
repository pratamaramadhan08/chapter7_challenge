import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';

const Carousel = () => (
    <div className="testimonial">
        <div>
        <div className="container-fluid grid-service grid-service1">
            <div className="row">
                <div className="col-sm text-center"> <br />
                    <h3><b>Testimonial</b></h3>
                    <p className="pt-2">Berbagai review positif dari para pelanggan kami</p>
                </div>
            </div>
        </div>
        <OwlCarousel items={2} className='owl-theme' loop center margin={10} nav>
        <div className=" item d-flex">
          <div className="row">
            <div className="col-md-4 mt-50">
              <img alt='' src="img/orang1.png" className="foto"/>
            </div>
            <div className="col-md-8 mt-20p">
              <img alt='' src="img/star.png" className="wi-22 bintang"/><br />
              <p className="mt-10 mkanan-30 tengahh">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                breiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit
                amet, consectetur adipiscing elit, sed do eiusmod”</p>
              <p className="bold mb-30 tengahh">John Dee 32, Bromo</p>
            </div>
          </div>
        </div>

        <div className=" item d-flex">
          <div className="row">
            <div className="col-md-4 mt-50">
              <img alt='' src="img/orang2.png" className="foto"/>
            </div>
            <div className="col-md-8 mt-20p">
              <img alt='' src="img/star.png" className="wi-22 bintang"/><br />
              <p className="mt-10 mkanan-30 tengahh">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                breiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit
                amet, consectetur adipiscing elit, sed do eiusmod”</p>
              <p className="bold mb-30 tengahh">John Dee 32, Bromo</p>
            </div>
          </div>
        </div>

        <div className=" item d-flex">
          <div className="row">
            <div className="col-md-4 mt-50">
              <img alt='' src="img/orang1.png" className="foto"/>
            </div>
            <div className="col-md-8 mt-20p">
              <img alt='' src="img/star.png" className="wi-22 bintang"/><br />
              <p className="mt-10 mkanan-30 tengahh">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                breiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit
                amet, consectetur adipiscing elit, sed do eiusmod”</p>
              <p className="bold mb-30 tengahh">John Dee 32, Bromo</p>
            </div>
          </div>
        </div>
        
      </OwlCarousel>;
    </div>
  </div>
    

)
export default Carousel