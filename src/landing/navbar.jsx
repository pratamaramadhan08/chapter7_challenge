import {Navbar, NavbarBrand,NavbarToggler, Collapse, Nav} from 'reactstrap'; 
 
 
const NavbarMenu = () => ( 
<div> 
  <Navbar 
    color="light" 
    expand="md" 
    light 
    container 
    header 
  > 
        <NavbarBrand href="/"> 
            <img className='logo' alt='' src='img/logo.png'/> 
        </NavbarBrand> 
        <NavbarToggler onClick={function noRefCheck(){}} /> 
        <Collapse navbar> 
            <Nav 
            className="ms-auto" 
            navbar-nav 
            > 
            <ul className="navbar-nav ms-auto"> 
                <li className="nav-item"> 
                <a className="nav-link text-black mrml-10" href="/cariMobil">Our Services</a> 
                </li> 
                <li className="nav-item"> 
                <a className="nav-link text-black mrml-10" href="#WhyUs">Why Us</a> 
                </li> 
                <li className="nav-item"> 
                <a className="nav-link text-black mrml-10" href="#Testi">Testimonial</a> 
                </li> 
                <li className="nav-item"> 
                <a className="nav-link text-black mrml-10" href="#FAQ">FAQ</a> 
                </li> 
                <li className="nav-item">
                <p><button><a style={{color:"aliceblue", textDecoration: "none"}} href="/register">Register</a></button></p> </li>
            </ul> 
            </Nav> 
        </Collapse> 
        </Navbar> 
</div> 
) 
 
export default NavbarMenu