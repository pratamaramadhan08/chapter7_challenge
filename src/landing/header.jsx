const Header = () => (
  <header className="klawu">
  <div className="container">
    <div className="row navbar navbar-expand-lg navbar-light bg-light">
      <div className="col-md-6">
        <br />
        <br />
        <h1 className="tulisan1">
          Sewa & Rental Mobil Terbaik di kawasan Karanganyar
        </h1>
        <p className="tulisan2">
          Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas
          terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu
          untuk sewa mobil selama 24 jam.
        </p>
        <p>
          <button>
            <a
              style={{ color: "aliceblue", textDecoration: "none" }}
              href="/Filtercar"
            >
              Mulai Sewa Mobil
            </a>
          </button>
        </p>
      </div>
      <div className="col-md-6 pe-auto">
        <img
          src="img/img_car.png"
          width="700px"
          alt="loading"
          className="img-fluid"
        />
      </div>
    </div>
  </div>
  </header>
  
)

export default Header
