import NavbarMenu from './navbar';
import Header from './header';
import Cewek from './cewek'
import Whyus from './whyus';
import Carousel from './carousel';
import Sewamobil from './sewamobil';
import Accordion from './accordion';
import Footer from './footer';

function Landing() {
    return (
      <div>
            <NavbarMenu />
            <Header />
            <Cewek/>
            <Whyus/>
            <Carousel/>
            <Sewamobil/>
            <Accordion/>
            <Footer/>
      </div>
    );
  }
  
  export default Landing;